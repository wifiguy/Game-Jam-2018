﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextScroller : MonoBehaviour {
    public GameObject panel1;
    public GameObject panel2;
    public GameObject text1;
    public GameObject text2;

    public Text Scroller;
    string[] test = new string[] { "BREAKING NEWS: STUDENT SLEEPS DURING GAME JAM!", "DODECOIN IS AT A RECORD BREAKING HIGH", "MISSILE ALERT IN HAWAII?", "DONALD TRUMP DENIES USING THE WORD 'PEOPLE' TO DESCRIBE IMMIGRANTS", "SCIENTIFIC SURVEY REVEALS STUDENTS ENJOY FREE FOOD"
        , "LOCAL BOY RUINS EVERYTHING FOREVER", "YOU ARE TRAPPED WITHIN THIS TV FOREVER", "BREAKING NEWS: I SAW A CUTE DOG TODAY", "HOT SINGLES IN YOUR AREA! CLICK NOW TO MEET UP!", "DO YOU NEED HELP WITH ANYTHING?", "WATCH OUT! THERE'S A HOLE IN FRONT OF YOU, DON'T FALL NOW"
        , "SCIENCE HAS CREATED A JAM MADE OF GAMES - HIGHLY NUTRITIOUS", "MAN THOUGHT HE SAW DINOSAUR TURNS OUT IT'S JUST A BIG ROCK", "ENERGY DRINKS PROVEN TO INCREASE BRAIN FUNCTIONALITY BY AT LEAST 100%", "NEW HOLO-LENS MAKES WEARERS LOOK FOOLISH WHILE POINTING AT THE AIR"
        , "WATER IS DANGEROUS! 100% OF PEOPLE THAT CONSUME WATER HAVE DIED", "TV WILL MAKE YOUR EYES GO SQUARE ... PROBABLY"};
    int i = 0;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("1"))
        {
            panel1.SetActive(true);
            panel2.SetActive(true);
            text1.SetActive(true);
            text2.SetActive(true);
            Scroller.text = test[i].ToString();
            if (i <  16)
            {
                i++;
            } else if (i >= 16)
            {
                i = 0;
            }
        }
        if (Input.GetKeyDown("2"))
        {
            panel1.SetActive(false);
            panel2.SetActive(false);
            text1.SetActive(false);
            text2.SetActive(false);
        }
	}
}
