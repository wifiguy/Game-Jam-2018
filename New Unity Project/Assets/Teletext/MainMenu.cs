﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public GameObject UI;
    public GameObject scan;
    public GameObject off;
    public GameObject menu;
    public GameObject timer;
    public GameObject scanLines;
    public GameObject scaLinesLight;
    public GameObject gameOver;

    public charactermoving character;

    GameOverAudio menuAudio;

    GameObject arrow;
    GameObject mainCont;
    GameObject playCont;
    GameObject optCont;
    GameObject exitCont;
    Text[] playModes; // list of Text components to be turned green when selected on the play screen
    Text[] exitText; // list of Text components to be turned green when selected on the exit screen
    Text[] optionsText;

    Text[] funcText;
    Text[] butText;

    GameObject controlsCont;
    GameObject creditsCont;

    int mainSel; //main screen's selected item - start at 1
    int subSel; //selected item on sub-menu screens - start at 1
    int menuNum; //which menu is active right now, 0-3: main, play, opt, exit
    int contNum; //which item is selected in the controls screen

    Vector3 arrPos; //arrow's current position

    List<string> locales;
    int localeTot;
    int locale;
    int scanLineNum;

    void Start()
    {

        gameOver.SetActive(false);
        menuAudio = GameObject.Find("AudioPlayer").GetComponent<GameOverAudio>();

        scaLinesLight.GetComponent<Renderer>().enabled = false;
        scanLines.GetComponent<Renderer>().enabled = true;
        menuNum = 0;
        mainSel = 1;
        subSel = 1;
        contNum = 1;
        arrow = GameObject.Find("Arrow");
        mainCont = GameObject.Find("MainContent");
        playCont = GameObject.Find("PlayContent");
        optCont = GameObject.Find("OptionsContent");
        exitCont = GameObject.Find("ExitContent");

        playModes = new Text[10];
        for (int i = 0; i < GameObject.Find("GameModes").GetComponent<Transform>().childCount; i++)
        {
            playModes[i] = GameObject.Find("GameModes").GetComponent<Transform>().GetChild(i).gameObject.GetComponent<Text>();
        }

        optionsText = new Text[4];
        for (int i = 0; i < GameObject.Find("OptionsText").GetComponent<Transform>().childCount; i++)
        {
            optionsText[i] = GameObject.Find("OptionsText").GetComponent<Transform>().GetChild(i).gameObject.GetComponent<Text>();
        }

        exitText = new Text[2];
        for (int i = 0; i < GameObject.Find("ExitText").GetComponent<Transform>().childCount; i++)
        {
            exitText[i] = GameObject.Find("ExitText").GetComponent<Transform>().GetChild(i).gameObject.GetComponent<Text>();
        }

        controlsCont = GameObject.Find("ControlsContent");
        creditsCont = GameObject.Find("CreditsContent");

        funcText = new Text[5];
        butText = new Text[5];
        for (int i = 0; i < 5; i++)
        {
            funcText[i] = GameObject.Find("Functions").GetComponent<Transform>().GetChild(i).gameObject.GetComponent<Text>();
            butText[i] = GameObject.Find("Buttons").GetComponent<Transform>().GetChild(i).gameObject.GetComponent<Text>();
        }


        controlsCont.SetActive(false);
        creditsCont.SetActive(false);

        playCont.SetActive(false);
        optCont.SetActive(false);
        exitCont.SetActive(false);

        locales = new List<string> { "Locale - English", "Locale - Something Else" };
        localeTot = 1;
        locale = 0;

        scanLineNum = 2;
    }

    void Update()
    {
        //////////////////////DOWN///////////////////////////
        if (Input.GetKeyDown(KeyCode.DownArrow) || (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0))
        {

            //if on the main screen
            if (menuNum == 0)
            {
                switch (mainSel)
                {
                    case 1:
                        mainSel = 2;
                        arrPos = arrow.GetComponent<RectTransform>().anchoredPosition;
                        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(arrPos.x, 90, arrPos.z);
                        break;
                    case 2:
                        mainSel = 3;
                        arrPos = arrow.GetComponent<RectTransform>().anchoredPosition;
                        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(arrPos.x, 35, arrPos.z);
                        break;
                    case 3:
                        mainSel = 1;
                        arrPos = arrow.GetComponent<RectTransform>().anchoredPosition;
                        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(arrPos.x, 145, arrPos.z);
                        break;
                }
            }
            //else if on play screen
            else if (menuNum == 1)
            {
                switch (subSel)
                {
                    case 1:
                        playModes[0].color = Color.white;
                        playModes[1].color = Color.green;
                        subSel = 2;
                        break;
                    case 2:
                        playModes[1].color = Color.white;
                        playModes[2].color = Color.green;
                        subSel = 3;
                        break;
                    case 3:
                        playModes[2].color = Color.white;
                        playModes[3].color = Color.green;
                        subSel = 4;
                        break;
                    case 4:
                        playModes[3].color = Color.white;
                        playModes[0].color = Color.green;
                        subSel = 1;
                        break;
                }
            }
            //else if on options screen
            else if (menuNum == 2)
            {
                //if on controls screen
                if (controlsCont.activeInHierarchy)
                {
                    /*
                    if(contNum == 5)
                    {
                        contNum = 1;
                        funcText[0].color = Color.green;
                        butText[0].color = Color.green;

                        funcText[4].color = Color.white;    
                        butText[4].color = Color.white;
                    }
                    else
                    {
                        funcText[contNum-1].color = Color.white;
                        butText[contNum-1].color = Color.white;
                        contNum++;
                        funcText[contNum - 1].color = Color.green;
                        butText[contNum - 1].color = Color.green;
                    }
                    */
                }
                //if not on credits screen
                else if (!creditsCont.activeInHierarchy)
                {
                    switch (subSel)
                    {
                        case 1:
                            optionsText[0].color = Color.white;
                            optionsText[1].color = Color.green;
                            subSel = 2;
                            break;
                        case 2:
                            optionsText[1].color = Color.white;
                            optionsText[2].color = Color.green;
                            subSel = 3;
                            break;
                        case 3:
                            optionsText[2].color = Color.white;
                            optionsText[0].color = Color.green;
                            subSel = 1;
                            break;
                    }
                }
            }

            //else if on exit screen
            else if (menuNum == 3)
            {
                switch (subSel)
                {
                    case 1:
                        exitText[0].color = Color.white;
                        exitText[1].color = Color.green;
                        subSel = 2;
                        break;
                    case 2:
                        exitText[1].color = Color.white;
                        exitText[0].color = Color.green;
                        subSel = 1;
                        break;
                }
            }
        }

        //////////////////////////UP///////////////////////////
        if (Input.GetKeyDown(KeyCode.UpArrow) || (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0))
        {

            if (menuNum == 0)
            {
                switch (mainSel)
                {
                    case 1:
                        mainSel = 3;
                        arrPos = arrow.GetComponent<RectTransform>().anchoredPosition;
                        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(arrPos.x, 35, arrPos.z);
                        break;
                    case 2:
                        mainSel = 1;
                        arrPos = arrow.GetComponent<RectTransform>().anchoredPosition;
                        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(arrPos.x, 145, arrPos.z);
                        break;
                    case 3:
                        mainSel = 2;
                        arrPos = arrow.GetComponent<RectTransform>().anchoredPosition;
                        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(arrPos.x, 90, arrPos.z);
                        break;
                }
            }
            //else if on play screen
            else if (menuNum == 1)
            {
                switch (subSel)
                {
                    case 1:
                        playModes[0].color = Color.white;
                        playModes[3].color = Color.green;
                        subSel = 4;
                        break;
                    case 2:
                        playModes[1].color = Color.white;
                        playModes[0].color = Color.green;
                        subSel = 1;
                        break;
                    case 3:
                        playModes[2].color = Color.white;
                        playModes[1].color = Color.green;
                        subSel = 2;
                        break;
                    case 4:
                        playModes[3].color = Color.white;
                        playModes[2].color = Color.green;
                        subSel = 3;
                        break;
                }
            }

            else if (menuNum == 2)
            {

                //if on controls screen
                if (controlsCont.activeInHierarchy)
                {
                    /*
                    if (contNum == 1)
                    {
                        contNum = 5;
                        funcText[0].color = Color.white;
                        butText[0].color = Color.white;

                        funcText[4].color = Color.green;
                        butText[4].color = Color.green;
                    }
                    else
                    {
                        funcText[contNum - 1].color = Color.white;
                        butText[contNum - 1].color = Color.white;
                        contNum--;
                        funcText[contNum - 1].color = Color.green;
                        butText[contNum - 1].color = Color.green;
                    }
                    */
                }
                //if not on credits page
                else if (!creditsCont.activeInHierarchy)
                {
                    switch (subSel)
                    {
                        case 1:
                            optionsText[0].color = Color.white;
                            optionsText[2].color = Color.green;
                            subSel = 3;
                            break;
                        case 2:
                            optionsText[1].color = Color.white;
                            optionsText[0].color = Color.green;
                            subSel = 1;
                            break;
                        case 3:
                            optionsText[2].color = Color.white;
                            optionsText[1].color = Color.green;
                            subSel = 2;
                            break;
                    }
                }
            }

            //else if on exit screen
            else if (menuNum == 3)
            {
                switch (subSel)
                {
                    case 1:
                        exitText[0].color = Color.white;
                        exitText[1].color = Color.green;
                        subSel = 2;
                        break;
                    case 2:
                        exitText[1].color = Color.white;
                        exitText[0].color = Color.green;
                        subSel = 1;
                        break;
                }
            }
        }

        ////////////////////////SUBMIT//////////////////////////
        if (Input.GetButtonDown("Submit"))
        {
            menuAudio.clk1.Play();
            //if on main screen
            if (menuNum == 0)
            {
                switch (mainSel)
                {
                    case 1:
                        //change to play screen
                        mainCont.SetActive(false);
                        playCont.SetActive(true);
                        for (int i = 0; i < 4; i++)
                        {
                            playModes[i].color = Color.white;
                        }
                        playModes[0].color = Color.green;
                        menuNum = 1;
                        break;
                    case 2:
                        //change to options screen
                        mainCont.SetActive(false);
                        optCont.SetActive(true);
                        for (int i = 0; i < 3; i++)
                        {
                            optionsText[i].color = Color.white;
                        }
                        optionsText[0].color = Color.green;
                        menuNum = 2;
                        break;
                    case 3:
                        //change to exit screen
                        mainCont.SetActive(false);
                        exitCont.SetActive(true);
                        exitText[0].color = Color.green;
                        exitText[1].color = Color.white;
                        menuNum = 3;
                        break;
                }
            }

            //else if on play screen
            else if (menuNum == 1)
            {
                character.runCon(subSel - 1);
                timer.BroadcastMessage("Activate");
                GameObject.Find("Menu").SetActive(false);
                GameObject.Find("Main Camera").GetComponent<Music>().Fsong.Play();
                gameObject.SetActive(false);
            }
            //else if on options screen
            else if (menuNum == 2)
            {
                switch (subSel)
                {
                    case 1:
                        //controls
                        controlsCont.SetActive(true);
                        break;
                    case 2:
                        //locale
                        if (locale == localeTot)
                        {
                            locale = 0;
                            optionsText[1].text = locales[locale];
                        }
                        else
                        {
                            locale++;
                            optionsText[1].text = locales[locale];
                        }
                        break;
                    case 3:
                        if(scanLineNum == 2){
                            scanLineNum = 1;
                            optionsText[2].text = "Toggle Scan Lines - Light";
                            scaLinesLight.GetComponent<Renderer>().enabled = true;
                            scanLines.GetComponent<Renderer>().enabled = false;

                        }
                        else if(scanLineNum == 1){
                            scanLineNum = 0;
                            optionsText[2].text = "Toggle Scan Lines - Off";
                            scaLinesLight.GetComponent<Renderer>().enabled = false;
                            scanLines.GetComponent<Renderer>().enabled = false;
                        }
                        else{
                            scanLineNum = 2;
                            optionsText[2].text = "Toggle Scan Lines - Full";
                            scaLinesLight.GetComponent<Renderer>().enabled = false;
                            scanLines.GetComponent<Renderer>().enabled = true;
                        }
                        break;
                    case 4:
                        //credits
                        creditsCont.SetActive(true);
                        break;
                }
            }
            //else if on exit screen
            else if (menuNum == 3)
            {
                switch (subSel)
                {
                    case 1:
                        //exit
                        //TODO exit func
                        StartCoroutine(turnoffTV());
                        Destroy(this);
                        break;
                    case 2:
                        //back to main screen
                        exitCont.SetActive(false);
                        mainCont.SetActive(true);
                        subSel = 1;
                        menuNum = 0;
                        break;
                }
            }
        }

        ////////////////////////////CANCEL////////////////////////
        if (Input.GetButtonDown("Cancel"))
        {
            menuAudio.clk2.Play();
            //if on exit screen, change to main screen
            if (exitCont.activeInHierarchy)
            {
                exitCont.SetActive(false);
                mainCont.SetActive(true);
                subSel = 1;
                menuNum = 0;
            }
            //if on options screen, change to main screen
            else if (optCont.activeInHierarchy)
            {
                //if on controls screen
                if (controlsCont.activeInHierarchy)
                {
                    controlsCont.SetActive(false);
                }
                //if on credits screen
                else if (creditsCont.activeInHierarchy)
                {
                    creditsCont.SetActive(false);
                }
                else
                {
                    optCont.SetActive(false);
                    mainCont.SetActive(true);
                    subSel = 1;
                    menuNum = 0;
                }
            }
            //if on play screen, change to main screen
            else if (playCont.activeInHierarchy)
            {
                playCont.SetActive(false);
                mainCont.SetActive(true);
                subSel = 1;
                menuNum = 0;
            }
            //if on main screen, change to exit screen
            else
            {
                mainCont.SetActive(false);
                exitCont.SetActive(true);
                menuNum = 3;
            }
        }

    }
    IEnumerator turnoffTV()
    {
        menu.GetComponent<MeshRenderer>().enabled = false;
        UI.GetComponent<Canvas>().enabled = false;
        MeshRenderer render = off.GetComponent<MeshRenderer>();
        render.enabled = true;
        scan.GetComponent<Renderer>().enabled = false;
        while (off.GetComponent<UnityEngine.Video.VideoPlayer>().isPlaying)
        {
            yield return null;
        }
        Application.Quit();

    }
}
