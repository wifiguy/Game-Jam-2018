﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charactermoving : MonoBehaviour
{

    public GameObject tvgame;
    public GameObject menu;
    public Sprite endScreen;
    public Rigidbody RB;
    public int Score = 0;
    public float velocity = 0f;
    public float sideSpeed = 2f;
    public float moveSpeed = 1f;
    public float jumpMod = 0.5f;
    public float ground = 0;
    public float gravity = 2.5f;
    public GameObject GameCam;
    public bool jump = true;
    private float SpeedMod = 1f;
    public float SprintSpeed = 2f;
    public float WalkSpeed = 0.5f;
    public bool Slow;
    public GameObject slowCharge;
    public GameObject sprintCharge;
    public bool gameComplete = false;
    public bool maxSprint = true;
    public bool maxSlow = true;
    public int maxSlowAmount = 100;
    public int maxSprintAmount = 100;
    public int slowAmount;
    public int sprintAmount;
    public int mode = 3;
    public Vector3 deathpos = new Vector3(2.3f, -2.24f, 2.185f);
    public Vector3 deathposcamera = new Vector3(9.4f, 1.3f, -5.9f);
    //0 = Normal, 1 = Speedrun, 2 = Hard, 3 = Matt Loves You <3

    void Start()
    {
        sprintAmount = maxSprintAmount;
        slowAmount = maxSlowAmount;
    }
    

    public void runCon(int mode)
    {
        this.mode = mode;
        if (mode == 0)
        {
            maxSprint = false;
            maxSlow = false;
        }
        else if (mode == 1)
        {
            maxSlowAmount = 0;
            maxSprintAmount = 0;
        }
        else if (mode == 2)
        {
            SpeedMod = 3f;
            jumpMod = 0.3f;
        }
        else if (mode == 3)
        {
            sideSpeed = Random.Range(1f, 5f);
            moveSpeed = Random.Range(0.5f, 3f);
            gravity = Random.Range(1f, 10f);
            SpeedMod = Random.Range(0.1f, 10f);
        }
        Activate();

    }

    void Activate()
    {
        StartCoroutine(moveLR());
        StartCoroutine(cameraMove());
        StartCoroutine(moveUD());
        StartCoroutine(death());
        StartCoroutine(Walk());
        StartCoroutine(Sprint());
        StartCoroutine(gameEnd());
    }

    void OnCollisionEnter(Collision theCollision)
    {
        Debug.Log(theCollision.gameObject.tag);
        if (theCollision.gameObject.tag == "Respawn")
        {
            velocity = 0;
            GameCam.transform.position = deathposcamera;
            transform.position = deathpos;
        }
        if (theCollision.gameObject.tag == "coin")
        {
            Score += 10;
        }
        if (theCollision.gameObject.tag == "ground")
        {
            jump = true;
        }
    }

    IEnumerator moveUD()
    {
        while (!gameComplete)
        {
            yield return new WaitForSeconds(0.001f);
            if ((Input.GetKey(KeyCode.W)| Input.GetKey(KeyCode.Space)) & jump)
            {
                jump = false;
                StartCoroutine(jumping());
            }
        }
    }
    IEnumerator jumping()
    {
        yield return new WaitForSeconds(0.001f);
        RB.AddForce(Vector3.up * Time.deltaTime * 40000f * jumpMod);
        //transform.Translate(Vector3.up * moveSpeed * jumpMod * 0.23f);
    }
    IEnumerator moveLR()
    {
        while (!gameComplete)
        {
            yield return new WaitForSeconds(0.001f);
            if (Input.GetKey(KeyCode.D) | Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Vector3.right * Time.deltaTime * moveSpeed * sideSpeed * SpeedMod * (1f + velocity));
                velocity += 0.01f;
            }
            else if (Input.GetKey(KeyCode.A) | Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(Vector3.left * Time.deltaTime * moveSpeed * sideSpeed * SpeedMod * -(-1f + velocity));
                velocity -= 0.01f;
            }
            else if (jump)
            {
                velocity = 0f;
            }
            else if (velocity < 0f)
            {
                transform.Translate(Vector3.right * Time.deltaTime * moveSpeed * sideSpeed * (velocity));
                velocity += 0.01f;
            }
            else if (velocity > 0f)
            {
                transform.Translate(Vector3.left * Time.deltaTime * moveSpeed * sideSpeed * -(velocity));
                velocity -= 0.01f;
            }
        }
    }
    IEnumerator cameraMove()
    {
        while (!gameComplete)
        {
            yield return new WaitForSeconds(0.001f);
            if (transform.position.x - GameCam.transform.position.x > 3)
            {
                GameCam.transform.Translate(Vector3.right * Time.deltaTime * sideSpeed * SpeedMod * moveSpeed * (1f + velocity));
            }
            if (transform.position.x - GameCam.transform.position.x < -3)
            {
                GameCam.transform.Translate(Vector3.left * Time.deltaTime * sideSpeed * SpeedMod * moveSpeed * -(-1f + velocity));

            }
        }
    }
    IEnumerator death()
    {
        while (!gameComplete)
        {

            yield return new WaitForSeconds(0.001f);
            if (transform.position.y < -5)
            {
                velocity = 0;
                GameCam.transform.position = new Vector3(5f, 1.3f, -5.9f);
                transform.position = deathpos;
                RB.velocity = Vector3.zero;
            }
        }
    }
    IEnumerator gameEnd(){
        while (!gameComplete){
            yield return new WaitForSeconds(0.001f);
            if (transform.position.x > 95){
                gameComplete = true;
            }
        }
        tvgame.SetActive(true);
        tvgame.GetComponent<MainMenu>().gameOver.SetActive(true);
        menu.SetActive(true);
        GameObject.Find("Main Camera").GetComponent<Music>().Fsong.Stop();
        GameObject.Find("Main Camera").GetComponent<Music>().Nsong.Stop();
        GameObject.Find("Main Camera").GetComponent<Music>().Ksong.Stop();
    }
    IEnumerator Sprint()
    {
        while (!gameComplete)
        {
            yield return new WaitForSeconds(0.001f);
            if (Input.GetKey(KeyCode.LeftShift) & ((maxSprint & sprintAmount > 0) | !maxSprint))
            {
                SpeedMod = SprintSpeed;
                sprintAmount -= 1;
            }
            else if (SpeedMod > 1f)
            {
                SpeedMod = 1f;
            }
            else
            {
                if (sprintAmount < maxSprintAmount)
                {
                    sprintAmount += 1;
                }
            }
            if (mode != 0) sprintCharge.transform.localScale = new Vector3(sprintAmount * 3 / maxSprintAmount, 1, 1);
        }
    }
    IEnumerator Walk()
    {
        while (!gameComplete)
        {
            yield return new WaitForSeconds(0.001f);
            if (Input.GetKey(KeyCode.LeftControl) & ((maxSlow & slowAmount > 0) | !maxSlow))
            {
                Slow = true;
                slowAmount -= 1;
                SpeedMod = WalkSpeed;
            }
            else if (SpeedMod < 1f)
            {
                Slow = false;
                SpeedMod = 1f;
            }
            else
            {
                if (slowAmount < maxSlowAmount)
                {
                    slowAmount += 1;
                }
            }
            if (mode != 0) slowCharge.transform.localScale = new Vector3(slowAmount * 3 / maxSlowAmount, 1, 1);
        }
    }
}

