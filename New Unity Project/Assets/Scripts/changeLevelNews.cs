﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeLevelNews : MonoBehaviour {

	public int levelCode = 1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void levelChange(int i){
		if (gameObject.GetComponent<MeshRenderer>()!=null)
		gameObject.GetComponent<MeshRenderer>().enabled = (i == levelCode);
		if (gameObject.GetComponent<BoxCollider>()!=null)
			gameObject.GetComponent<BoxCollider>().enabled = (i == levelCode);
		if (gameObject.GetComponent<MeshCollider>()!=null)
			gameObject.GetComponent<MeshCollider>().enabled = (i == levelCode);
	}
}
