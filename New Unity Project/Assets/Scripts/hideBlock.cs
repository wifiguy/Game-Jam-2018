﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hideBlock : MonoBehaviour {

	public bool kitchen;
	public bool news;
	public bool nature;
	private int currentState;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void changeState(int stateChange){
		switch(stateChange){
			case 1:
				gameObject.GetComponent<Renderer>().enabled = kitchen;
				gameObject.GetComponent<Collider>().enabled = kitchen;
				break;
			case 2:
				gameObject.GetComponent<Renderer>().enabled = news;
				gameObject.GetComponent<Collider>().enabled = news;
				break;
			case 3:
				gameObject.GetComponent<Renderer>().enabled = nature;
				gameObject.GetComponent<Collider>().enabled = nature;
				break;
			default:
				break;
			}
		}


	}
