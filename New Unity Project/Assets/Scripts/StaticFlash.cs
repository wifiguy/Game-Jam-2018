﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticFlash : MonoBehaviour {

    // Use this for initialization
    
    public GameObject canvas;
    public AudioSource sound;

    public GameObject worldController;

    public GameObject UI = null;

    public GameObject scan = null;
    
    public GameObject Lightning = null;

    MeshRenderer render;

    public int turnOff;
    public bool turnoff;

    void Start () {
       
        MeshRenderer render = gameObject.GetComponent<MeshRenderer>();
        render.enabled = false;
        worldController.BroadcastMessage("changeState",1);
        if (Lightning!=null)
            Lightning.SetActive(true);
	}
    // Update is called once per frame
    
	void Update () {if (!turnoff){
		if (Input.GetKeyDown("1"))
        {
            StartCoroutine(Static());
            worldController.BroadcastMessage("changeState",1);
            if (true){//needs changing to detect
            }
        }
        if (Input.GetKeyDown("2"))
        {
            worldController.BroadcastMessage("changeState",2);
            StartCoroutine(Static());
        }
        if (Input.GetKeyDown("3"))
        {
            worldController.BroadcastMessage("changeState",3);
            StartCoroutine(Static());
        }}
        else {
            if (Input.GetKeyDown(KeyCode.P))
            StartCoroutine(turnoffTV());
            
        }
    }

    IEnumerator Static()
    {
        MeshRenderer render = gameObject.GetComponent<MeshRenderer>();
        Lightning.SetActive(true);
        Lightning.GetComponent<ParticleSystem>().Emit(100);
        yield return new WaitForSeconds(0.1f);
        render.enabled = true;
        canvas.SetActive(false);
        sound.Play();
        yield return new WaitForSeconds(0.5f);
        Lightning.GetComponent<ParticleSystem>().Emit(100);
        render.enabled = false;
                
        canvas.SetActive(true);
    }
        IEnumerator turnoffTV()
    {
        UI.GetComponent<Canvas>().enabled = false;
        MeshRenderer render = gameObject.GetComponent<MeshRenderer>();
        render.enabled = true;
        scan.GetComponent<Renderer>().enabled = false;
        while (gameObject.GetComponent<UnityEngine.Video.VideoPlayer>().isPlaying){
            yield return null;
        }
        Application.Quit();
    }
}


