﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour {
	public int currentMS = 0;
	public int currentSeconds = 0;
	public int currentMinutes = 0;
	public Text textTime;
	// Use this for initialization
	void Activate () { 
		StartCoroutine(time());
		}
	IEnumerator time(){
		while (true){
			yield return new WaitForSeconds(0.01f);
			if (currentSeconds > 59){
				currentMinutes+=1;
				currentSeconds = 0;
			}if (currentMS > 99){
				currentSeconds+=1;
				currentMS = 0;
			}
			currentMS+=1;
			if (currentSeconds <10 & currentMS < 10){
				textTime.text = string.Format("{0}:0{1}:0{2}",currentMinutes, currentSeconds,currentMS);}
			else if (currentMS < 10){
				textTime.text = string.Format("{0}:{1}:0{2}",currentMinutes, currentSeconds,currentMS);}
			else if (currentSeconds<10){
				textTime.text = string.Format("{0}:0{1}:{2}",currentMinutes, currentSeconds,currentMS);
			
			}else{
			textTime.text = string.Format("{0}:{1}:{2}",currentMinutes, currentSeconds,currentMS);
			}
		}
	}
}
