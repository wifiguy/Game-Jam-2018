﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverAudio : MonoBehaviour {

    public AudioSource src;
    public AudioClip beg;
    public AudioClip mid;
    public AudioClip end;
    public AudioSource clk1;
    public AudioSource clk2;
    public AudioSource fad;
    public bool endNoises;
    
    // Use this for initialization
	void Start () {
        clk1 = GameObject.Find("Click1").GetComponent<AudioSource>();
        clk2 = GameObject.Find("Click2").GetComponent<AudioSource>();
        fad = GameObject.Find("Fade").GetComponent<AudioSource>();
        StartCoroutine(playManager());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator playManager()
    {
        yield return new WaitForSeconds(2);
        src.clip = mid;
        src.Play();
        while (!endNoises)
        {
            yield return new WaitForSeconds(6);
        }
        src.loop = false;
        src.Play();
        src.clip = end;
    }
}
