﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticFlash1 : MonoBehaviour {

    // Use this for initialization

    public GameObject cube;
    public AudioSource sound;

    void Start () {
        MeshRenderer render = gameObject.GetComponent<MeshRenderer>();
        render.enabled = false;
	}

    // Update is called once per frame
    
	void Update () {
		if (Input.GetKeyDown("1"))
        {
            StartCoroutine(Static());
            cube.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (Input.GetKeyDown("2"))
        {
            StartCoroutine(Static());
            cube.GetComponent<Renderer>().material.color = Color.red;
        }
        if (Input.GetKeyDown("3"))
        {
            StartCoroutine(Static());
            cube.GetComponent<Renderer>().material.color = Color.green;
        }
    }

    IEnumerator Static()
    {
        MeshRenderer render = gameObject.GetComponent<MeshRenderer>();
        render.enabled = true;
        sound.Play();
        yield return new WaitForSeconds(1f);
        render.enabled = false;
    }
}


