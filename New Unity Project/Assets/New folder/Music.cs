﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {

    public AudioSource Ksong;
    public AudioSource Nsong;
    public AudioSource Fsong;

	// Use this for initialization
	void Start () {
        Ksong.loop = true;
        Nsong.loop = true;
        Fsong.loop = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("3"))
        {
            if (Ksong.isPlaying)
            {
                NewsPause();
                ForestPause();
            } else
            {
                KitchenPlay();
                NewsPause();
                ForestPause();
            }
        }
        if (Input.GetKeyDown("1"))
        {
            if (Nsong.isPlaying)
            {
                KitchenPause();
                ForestPause();
            }
            else
            {
                NewsPlay();
                KitchenPause();
                ForestPause();
            }
        }
        if (Input.GetKeyDown("2"))
        {
            if (Fsong.isPlaying)
            {
                KitchenPause();
                NewsPause();
            }
            else
            {
                ForestPlay();
                KitchenPause();
                NewsPause();
            }
        }
    }

    public void KitchenPlay()
    {
        Ksong.Play(); 
    }
    public void KitchenPause()
    {
        Ksong.Pause();
    }

    public void NewsPlay()
    {
        Nsong.Play();
    }
    public void NewsPause()
    {
        Nsong.Pause();
    }

    public void ForestPlay()
    {
        Fsong.Play();
    }
    public void ForestPause()
    {
        Fsong.Pause();
    }
}
